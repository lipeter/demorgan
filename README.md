# Augustus De Morgan, *Formal Logic*

Reconstruction of Augustus De Morgan's logical system, as analyzed in:

J. L. Gastaldi. **“De Morgan’s Laws: The Placeof Duality in the Emergence of Formal Logic”**.In: *Duality in 19th and 20th century mathematical thinking*. Ed. by R. Krömer, E. Haffner, and K. Volkert. Birkhäuser, forthcoming.
